# 2018 MEOPAR Python tutorial

2018 MEOPAR Python tutorial - Halifax, Nova Scotia

-------------------------------------

### Installation instructions

Prior to workshop...

1) Download and install [Anaconda with Python 3.6](https://www.anaconda.com/download/) OR  [Anaconda with Python 3.6](https://conda.io/miniconda.html)

2) Install the required modules by typing in **Anaconda Promt**:

- `conda install -c anaconda notebook`

- `conda install pandas matplotlib jupyter`

- `conda install -c conda-forge basemap basemap-data-hires netcdf4`